local ReplicatedStorage = game:GetService("ReplicatedStorage")

local import = require(ReplicatedStorage.Code.Import)
local Logger = import("shared/Logger")
local FastSpawn = import("shared/FastSpawn")

local log = Logger.new("Networking")
local eventsFolder = ReplicatedStorage.Events

local function GetEvent(name, callback)
	FastSpawn(function()
		local event = eventsFolder:WaitForChild(name, 1)
		if not event then
			log:Warn("No event with name %q. Did you forget to create it on the server?", name)
			return
		end
		callback(event)
	end)
end

local Networking = {}

function Networking:OnClientEvent(name, callback)
	GetEvent(name, function(event)
		event.OnClientEvent:Connect(callback)
	end)
end

function Networking:FireServer(name, ...)
	local args = {...}
	GetEvent(name, function(event)
		event:FireServer(unpack(args))
	end)
end

return Networking
