local ReplicatedStorage = game:GetService("ReplicatedStorage")

local import = require(ReplicatedStorage.Code.Import)
local Networking = import("~/Networking")
local Logger = import("shared/Logger")

local log = Logger.new("PlayerData")
local dataLoadedEvent = Instance.new("BindableEvent")
local data

Networking:OnClientEvent("SetPlayerData", function(_data)
	log:Debug("Player data received from the server")
	data = _data
	dataLoadedEvent:Fire()
end)

local PlayerData = {}

function PlayerData:Get()
	if not data then
		dataLoadedEvent.Event:Wait()
	end
	return data
end

return PlayerData
