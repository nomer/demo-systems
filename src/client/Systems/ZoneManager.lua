local ReplicatedStorage = game:GetService("ReplicatedStorage")
local Players = game:GetService("Players")
local Workspace = game:GetService("Workspace")

local import = require(ReplicatedStorage.Code.Import)
local Networking = import("~/Networking")
local Zones = import("shared/Zones")
local Logger = import("shared/Logger")

local log = Logger.new("ZoneManager")
local localPlayer = Players.LocalPlayer
local playerZoneState = {}
local myZoneCache

local ZoneManager = {}

local function RefreshPlayerOcclusion()
	local myZone = ZoneManager:GetMyZone()
	for _, player in ipairs(Players:GetPlayers()) do
		if player ~= localPlayer then
			if ZoneManager:GetPlayerZone(player) == myZone then
				player.Parent = Workspace
			else
				player.Parent = ReplicatedStorage
			end
		end
	end
end

local function RefreshZoneOcclusion()
	local newZone = ZoneManager:GetMyZone()
	if newZone ~= myZoneCache then
		local newZoneData = Zones[newZone]
		local prevZoneData = Zones[myZoneCache]
		if prevZoneData then
			local instance = prevZoneData.getInstance()
			if instance then
				instance.Parent = ReplicatedStorage.Zones
			else
				log:Warn("Zone %s has no instance", myZoneCache)
			end
		end
		if newZoneData then
			local instance = newZoneData.getInstance()
			if instance then
				instance.Parent = ReplicatedStorage.Zones
			else
				log:Warn("Zone %s has no instance", newZone)
			end
		end
		myZoneCache = newZone
	end
end

Networking:OnClientEvent("SetPlayerZone", function(player, newZone)
	log:Debug("Received atomic zone update from server: player %q is now in zone %q", player.Name, tostring(newZone))
	playerZoneState[tostring(player.UserId)] = newZone
	RefreshPlayerOcclusion()
	RefreshZoneOcclusion()
end)

Networking:OnClientEvent("SetAllPlayerZones", function(zoneState)
	log:Debug("Received whole zone state from server")
	playerZoneState = zoneState
	RefreshPlayerOcclusion()
	RefreshZoneOcclusion()
end)

function ZoneManager:GetMyZone()
	return playerZoneState[tostring(localPlayer.UserId)]
end

function ZoneManager:GetPlayerZone(player)
	return playerZoneState[tostring(player.UserId)]
end

return ZoneManager
