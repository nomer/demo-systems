local ReplicatedStorage = game:GetService("ReplicatedStorage")

local MAX_SYSTEM_REQUIRE_TIME = 0.01
local MAX_SYSTEM_START_TIME = 0.05

local import = require(ReplicatedStorage.Code.Import)
local Logger = import("shared/Logger")

local log = Logger.new("Main")
local systemsFolder = script.Parent.Systems
local systems = {}

local mainStart = tick()

for _, module in ipairs(systemsFolder:GetChildren()) do
	local start = tick()
	systems[module.Name] = require(module)
	local t = tick() - start
	if t >= MAX_SYSTEM_REQUIRE_TIME then
		log:Warn("System %q took %.4f to require", module.Name, t)
	end
end

for name, system in pairs(systems) do
	if system.Start then
		local start = tick()
		system:Start()
		local t = tick() - start
		if t >= MAX_SYSTEM_START_TIME then
			log:Warn("System %q took %.4f to start", name, t)
		end
	end
end

local totalTime = tick() - mainStart

log:Debug("Systems required and started in %.4f", totalTime)
