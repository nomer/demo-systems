local ReplicatedStorage = game:GetService("ReplicatedStorage")
local Players = game:GetService("Players")

local import = require(ReplicatedStorage.Code.Import)
local Logger = import("shared/Logger")

local log = Logger.new("Networking")

local events = {}
local eventsFolder = Instance.new("Folder")
eventsFolder.Name = "Events"
eventsFolder.Parent = ReplicatedStorage

local Networking = {}

function Networking:MakeEvent(name)
	if events[name] then
		log:Warn("There is already an event with name %q", name)
		return false
	end
	local event = Instance.new("RemoteEvent")
	event.Name = name
	event.Parent = eventsFolder
	events[name] = event
	return true
end

function Networking:OnServerEvent(name, callback)
	local event = events[name]
	if not event then
		log:Warn("No event with name %q", name)
		return false
	end
	return event.OnClientEvent:Connect(callback)
end

function Networking:FireClient(name, player, ...)
	local event = events[name]
	if not event then
		log:Warn("No event with name %q", name)
		return false
	end
	event:FireClient(player, ...)
	return true
end

function Networking:FireAllClients(name, ...)
	local event = events[name]
	if not event then
		log:Warn("No event with name %q", name)
		return false
	end
	event:FireAllClients(...)
	return true
end

function Networking:FireClients(name, players, ...)
	local event = events[name]
	if not event then
		log:Warn("No event with name %q", name)
		return false
	end
	for _, player in ipairs(players) do
		event:FireClient(player, ...)
	end
end

function Networking:FireAllBut(name, player, ...)
	local event = events[name]
	if not event then
		log:Warn("No event with name %q", name)
		return false
	end
	for _, _player in ipairs(Players:GetPlayers()) do
		if _player ~= player then
			event:FireClient(_player, ...)
		end
	end
end

return Networking
