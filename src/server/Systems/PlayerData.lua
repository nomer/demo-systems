-- This module assumes the game is published. MockDataStoreService should be
-- used to cover the case of the game not being published.
-- https://devforum.roblox.com/t/t/140654
local DataStoreService = game:GetService("DataStoreService")
local Players = game:GetService("Players")
local RunService = game:GetService("RunService")
local ReplicatedStorage = game:GetService("ReplicatedStorage")

local DATASTORE_NAME_DEV = "Development"
local DATASTORE_NAME_PROD = "Production"
local DATASTORE_SCOPE = "Player_%d"

local import = require(ReplicatedStorage.Code.Import)
local Networking = import("~/Networking")
local Logger = import("shared/Logger")
local GetPlayerDataTemplate = import("shared/GetPlayerDataTemplate")
local Signal = import("shared/Signal")
local FastSpawn = import("shared/FastSpawn")

Networking:MakeEvent("SetPlayerData")

local dataSavedSignal = Signal.new()
local log = Logger.new("PlayerData")
local playerDataCache = {}
local playerDataLastSaved = {}
local getRequests = {}
local saveRequests = {}
local saveQueued = {}

local function DeepCopy(a)
	if type(a) == "table" then
		local new = {}
		for i, v in pairs(a) do
			new[DeepCopy(i)] = DeepCopy(v)
		end
		return new
	end
	return a
end

local function DeepEquals(a, b)
	if type(a) == "table" then
		if type(b) == "table" then
			for i, v in pairs(a) do
				if not DeepEquals(v, b[i]) then
					return false
				end
			end
			for i, v in pairs(b) do
				if not DeepEquals(v, a[i]) then
					return false
				end
			end
			return true
		end
		return false
	end
	return a == b
end

local function GetDataStoreName()
	return RunService:IsStudio() and DATASTORE_NAME_DEV or DATASTORE_NAME_PROD
end

local function GetDataStore(userId)
	local scope = DATASTORE_SCOPE:format(userId)
	return DataStoreService:GetDataStore(GetDataStoreName(), scope)
end

local function GetOrderedDataStore(userId)
	local scope = DATASTORE_SCOPE:format(userId)
	return DataStoreService:GetOrderedDataStore(GetDataStoreName(), scope)
end

local function CanBeRetried(result)
	-- https://developer.roblox.com/en-us/articles/Datastore-Errors
	-- These are the codes we want to retry on.
	local codes = {
		"301", -- GetAsync request dropped. Request was throttled, but throttled request queue was full.
		"302", -- SetAsync request dropped. Request was throttled, but throttled request queue was full.
		"305", -- GetSorted request dropped. Request was throttled, but throttled request queue was full.
		"502", -- API Services rejected request with error: X.
		"request was throttled, but throttled queue was full",
		"API Services rejected request with error:",
		"simulated error"
	}
	-- These are errors we don't want to retry on but may still contain a
	-- retryable code.
	local exceptions = {
		"HTTP 0" -- Studio does not have access to DataStoreService.
	}
	for _, code in ipairs(codes) do
		if result:find(code, 1, true) then
			local exceptionFound = false
			for _, exception in ipairs(exceptions) do
				if result:find(exception, 1, true) then
					exceptionFound = true
					break
				end
			end
			if not exceptionFound then
				log:Info("Result %q contained code %q", result, code)
				return true
			end
		end
	end
	return false
end

-- This utility function retries a function that calls DataStore methods because
-- DataStores may fail *temporarily*. Retrying as many times as necessary under
-- "retryable" conditions (seen above, function CanBeRetried) will keep the game
-- as tolerant to sudden DataStore failures as possible.
local function RetryDataStoreFunction(func, funcName)
	log:Debug("Initial call of %q", funcName)
	local success, result = pcall(func)
	if not success then
		repeat
			if not CanBeRetried(result) then
				log:Error("Calling %q failed and cannot be retried: %s", funcName, result)
				return false, result
			end
			log:Warn("Calling %q failed: %s", funcName, tostring(result))
			log:Warn("Retrying %q in 5 seconds...", funcName)
			wait(5)
			success, result = pcall(func)
		until success
	end
	log:Debug("Calling %q successful", funcName)
	return true, result
end

local function FetchPlayerData(userId)
	-- Data is stored in the same fashion used in DataStore2
	-- (https://devforum.roblox.com/t/t/136317)
	-- Each player has a corresponding OrderedDataStore storing incremental
	-- keys which are used to index data in a DataStore. Each time data is
	-- saved, the key is incremented. To retrieve data, we get the highest key
	-- in the OrderedDataStore and use it to retrieve data from the player's
	-- DataStore.
	local orderedDataStore = GetOrderedDataStore(userId)

	local function GetMostRecentKey()
		return orderedDataStore:GetSortedAsync(false, 1):GetCurrentPage()[1]
	end

	local keySuccess, keyResult = RetryDataStoreFunction(GetMostRecentKey, "GetMostRecentKey")
	if not keySuccess then
		-- DataStore call failed and can not be retried.
		return false, keyResult
	elseif not keyResult or not keyResult.value then
		-- DataStore call succeeded, player has no previous data.
		return true, nil
	end

	local mostRecentKey = keyResult.value

	local dataStore = GetDataStore(userId)
	local function GetData()
		return dataStore:GetAsync(mostRecentKey)
	end

	local dataSuccess, dataResult = RetryDataStoreFunction(GetData, "GetData")
	if not dataSuccess then
		-- DataStore call failed and can not be retried.
		return false, dataResult
	end

	-- DataStore call succeeded.
	return true, dataResult
end

local function SavePlayerData(userId, data)
	-- We don't need to save if the data hasn't changed at all.
	local lastSaved = playerDataLastSaved[userId]
	if lastSaved and DeepEquals(data, lastSaved) then
		return true
	end

	local dataStore = GetDataStore(userId)
	local orderedDataStore = GetOrderedDataStore(userId)

	-- We need to copy the data before saving it to isolate it from any changes
	-- during saving. If we didn't, our changed check above might fail
	-- incorrectly.
	local dataCopy = DeepCopy(data)

	-- We need to keep the old key in memory in case the save fails, at which
	-- point we revert it.
	local oldKey = dataCopy._saveNum
	local newKey = oldKey + 1

	dataCopy._saveNum = newKey

	local function SetData()
		dataStore:SetAsync(newKey, dataCopy)
	end

	-- We're going to try saving data to the DataStore first. If we try saving
	-- to the OrderedDataStore first and then saving to the DataStore fails, the
	-- OrderedDataStore will point to invalid data.
	local dataSuccess, dataResult = RetryDataStoreFunction(SetData, "SetData")
	if not dataSuccess then
		-- DataStore call failed and can not be retried. Revert the key change.
		dataCopy._saveNum = oldKey
		return false, dataResult
	end

	local function SetMostRecentKey()
		orderedDataStore:SetAsync(newKey, newKey)
	end

	local keySuccess, keyResult = RetryDataStoreFunction(SetMostRecentKey, "SetMostRecentKey")
	if not keySuccess then
		-- DataStore call failed and can not be retried. Revert the key change.
		dataCopy._saveNum = oldKey
		return false, keyResult
	end

	-- Save is successful at this point, so we're going to set lastSaved to the
	-- copy and push the key change to the main table. If any changes were made
	-- during saving they can be properly saved.
	playerDataLastSaved[userId] = dataCopy
	data._saveNum = newKey

	return true
end

local PlayerData = {}

function PlayerData:Get(player)
	local userId = player.UserId
	if playerDataCache[userId] then
		return playerDataCache[userId]
	end
	if getRequests[userId] then
		return getRequests[userId]:Wait()
	end

	local getRequestEvent = Instance.new("BindableEvent")
	getRequests[userId] = getRequestEvent

	local success, data = FetchPlayerData(userId)

	if not success then
		return
	elseif not data then
		data = GetPlayerDataTemplate()
	end

	getRequestEvent:Fire(data)
	getRequests[userId] = nil
	Networking:FireClient("SetPlayerData", player, data)
	return data
end

function PlayerData:WaitForSaveFinished(player)
	if saveRequests[player.UserId] then
		return saveRequests[player.UserId].Event:Wait()
	end
end

function PlayerData:Save(player)
	local userId = player.UserId
	if not playerDataCache[userId] then
		return false
	end
	if getRequests[userId] then
		return false
	end
	if saveRequests[userId] then
		-- This shallow queue ensures that any save request made *during* a save
		-- request will run after the initial save request. Simply returning the
		-- results of the last save request does not guarantee that a request
		-- made after modifying data during another request will indeed save.
		if saveQueued[userId] then
			self:WaitForSaveFinished(player)
			return self:WaitForSaveFinished(player)
		else
			saveQueued[userId] = true
			self:WaitForSaveFinished(player)
			saveQueued[userId] = false
		end
	end

	local saveRequestEvent = Instance.new("BindableEvent")
	saveRequests[userId] = saveRequestEvent

	local success = SavePlayerData(userId, playerDataCache[userId])

	saveRequestEvent:Fire(success)
	saveRequests[userId] = nil
	dataSavedSignal:Fire()
	return success
end

local function PlayerAdded(player)
	PlayerData:Get(player)
	while wait(5 * 60) do
		if not player.Parent then
			break
		end
		PlayerData:Save(player)
	end
end

local function PlayerRemoving(player)
	PlayerData:Save(player)
	playerDataLastSaved[player.UserId] = nil
	playerDataCache[player.UserId] = nil
end

function PlayerData:Start()
	Players.PlayerAdded:Connect(PlayerAdded)
	for _, player in ipairs(Players:GetPlayers()) do
		FastSpawn(PlayerAdded, player)
	end

	Players.PlayerRemoving:Connect(PlayerRemoving)

	game:BindToClose(function()
		-- PlayerRemoving fires before BindToClose, this is just for sanity.
		for _, player in ipairs(Players:GetPlayers()) do
			if not saveRequests[player.UserId] then
				FastSpawn(PlayerRemoving, player)
			end
		end
		while next(saveRequests) do
			dataSavedSignal:Wait()
		end
	end)
end

return PlayerData
