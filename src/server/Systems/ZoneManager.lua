local ReplicatedStorage = game:GetService("ReplicatedStorage")
local Players = game:GetService("Players")

local import = require(ReplicatedStorage.Code.Import)
local Networking = import("~/Networking")
local Signal = import("shared/Signal")

Networking:MakeEvent("SetPlayerZone")
Networking:MakeEvent("SetAllPlayerZones")

local zoneEnteredSignals = {}
local zoneLeftSignals = {}
local playerZoneChangedSignals = {}
-- This table is going to be sent across the network. Its keys will be
-- stringified player UserIds instead of numbers because non-sequential arrays
-- do not get transmitted.
local playerZoneState = {}

local function FireIfExists(signal, ...)
	if signal then
		signal:Fire(...)
	end
end

local ZoneManager = {}

function ZoneManager:SetPlayerZone(player, zoneName)
	if playerZoneState[tostring(player.UserId)] ~= zoneName then
		local previousZone = playerZoneState[tostring(player.UserId)]
		playerZoneState[tostring(player.UserId)] = zoneName
		FireIfExists(playerZoneChangedSignals[player], zoneName)
		FireIfExists(zoneEnteredSignals[zoneName], player, previousZone)
		FireIfExists(zoneLeftSignals[previousZone], player, zoneName)
		Networking:FireAllClients("SetPlayerZone", player, zoneName)
	end
end

function ZoneManager:GetPlayerZone(player)
	return playerZoneState[tostring(player.UserId)]
end

function ZoneManager:GetPlayerZoneChangedSignal(player)
	local signal = playerZoneChangedSignals[player]
	if not signal then
		signal = Signal.new()
		playerZoneChangedSignals[player] = signal
	end
	return signal
end

function ZoneManager:GetZoneEnteredSignal(zoneName)
	local signal = zoneEnteredSignals[zoneName]
	if not signal then
		signal = Signal.new()
		zoneEnteredSignals[zoneName] = signal
	end
	return signal
end

function ZoneManager:GetZoneLeftSignal(zoneName)
	local signal = zoneLeftSignals[zoneName]
	if not signal then
		signal = Signal.new()
		zoneLeftSignals[zoneName] = signal
	end
	return signal
end

local function PlayerAdded(player)
	ZoneManager:SetPlayerZone(player, "foo")
	Networking:FireClient("SetAllPlayerZones", player, playerZoneState)
end

function ZoneManager:Start()
	Players.PlayerAdded:Connect(PlayerAdded)
	for _, player in ipairs(Players:GetPlayers()) do
		PlayerAdded(player)
	end
end

return ZoneManager
