local function FastSpawn(callback, ...)
	local event = Instance.new("BindableEvent")
	event.Event:Connect(callback)
	event:Fire(...)
	event:Destroy()
end

return FastSpawn
