local ReplicatedStorage = game:GetService("ReplicatedStorage")
local RunService = game:GetService("RunService")
local ServerScriptService = game:GetService("ServerScriptService")
local StarterPlayer = game:GetService("StarterPlayer")

local aliases = {
	shared = ReplicatedStorage.Code,
	["~"] = RunService:IsServer() and ServerScriptService.Code or StarterPlayer.StarterPlayerScripts.Code
}

local activeImports = {}

local function import(path)
	local segments = path:split("/")
	local caller = getfenv(2).script
	local current = caller

	activeImports[caller] = true

	for i, name in ipairs(segments) do
		if name == ".." then
			current = i == 1 and current.Parent.Parent or current.Parent
		elseif name == "." then
			current = i == 1 and current.Parent or current
		else
			if i == 1 and aliases[name] then
				current = aliases[name]
			else
				current = current:FindFirstChild(name)
				if not current then
					error(("Error importing: %q not found in %q"):format(name, path), 0)
				end
			end
		end
	end

	if current:IsA("ModuleScript") then
		if activeImports[current] then
			error(debug.traceback(2, "Circular import detected"))
		end
		activeImports[caller] = nil
		return require(current)
	else
		activeImports[caller] = nil
		return current
	end
end

return import
