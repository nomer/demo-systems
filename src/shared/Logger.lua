local RunService = game:GetService("RunService")

local LOG_MESSAGE_TEMPLATE = "[%s] [%s] %s: %s"

local environmentName = RunService:IsServer() and "Server" or "Client"
local outputFunc = print
local outputLevel = 5

local Logger = {}
Logger.__index = Logger

function Logger.new(scope)
	local self = setmetatable({}, Logger)
	self._scope = scope
	return self
end

-- Instead of manually declaring all of this logic for each function we're just
-- going to use a higher-order function containing all of the shared logic.
local function MakeLogFunction(severity, severityInt)
	local template = LOG_MESSAGE_TEMPLATE:format(environmentName, severity, "%s", "%s")
	return function(self, msg, ...)
		if severityInt <= outputLevel then
			local formattedMsg = msg:format(...)
			outputFunc(template:format(self._scope, formattedMsg))
		end
	end
end

Logger.Info = MakeLogFunction("INFO", 5)

Logger.Debug = MakeLogFunction("DEBUG", 4)

Logger.Warn = MakeLogFunction("WARN", 3)

Logger.Error = MakeLogFunction("ERROR", 2)

Logger.Critical = MakeLogFunction("CRITICAL", 1)

return Logger
