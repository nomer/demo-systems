local Signal = {}
Signal.__index = Signal

function Signal.new()
	local self = setmetatable({}, Signal)
	self._instance = Instance.new("BindableEvent")
	return self
end

function Signal:Connect(callback)
	return self._instance.Event:Connect(callback)
end

function Signal:Fire(...)
	self._instance:Fire(...)
end

function Signal:Wait()
	return self._instance.Event:Wait()
end

return Signal
