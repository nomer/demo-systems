local ReplicatedStorage = game:GetService("ReplicatedStorage")

-- A function to retrieve the zone instance is needed instead of just referring
-- to the instance (i.e., ReplicatedStorage.Zones.Foo) in case we're in a
-- minimal testing environment and some zones don't exist.
return {
	foo = {
		unlocalizedText = "_zoneFoo",
		getInstance = function()
			local zones = ReplicatedStorage:FindFirstChild("Zones")
			return zones and zones:FindFirstChild("Foo")
		end
	}
}
